$(document).ready(
	async function () {
		const canvas = document.getElementById("avatar-canvas");
		const context = canvas.getContext("2d");
		
		const overlay = new Image();
		
		const overlayLoadPromise = new Promise(
			resolve => {
				overlay.onload = function() { resolve() };
				
				overlay.src = "assets/images/overlay.webp";
			}
		);
		
		let image = new Image();
		
		const imageLoadPromise = new Promise(
			resolve => {
				image.onload = function() { resolve() };
				
				image.src = "assets/images/default-avatar.webp";
			}
		);
		
		await overlayLoadPromise;
		await imageLoadPromise;
		
		function redraw() {
			// User image
			
			// https://github.com/DonkeyDushan/piratilol/blob/main/src/js/index.js
			// Thanks to DonkeyDushan, the guy who made the joke 2021 campaign generator :D
			
			const imageScaleX = canvas.width / image.width;
			const imageScaleY = canvas.height / image.height;
			
			const imageScale = Math.max(imageScaleX, imageScaleY);
			
			// https://stackoverflow.com/a/8529655
			// Thanks to alex!
			context.setTransform(
				imageScale,
				0, 0,
				imageScale,
				(canvas.width - image.width * imageScale) / 2,
				(canvas.height - image.height * imageScale) / 2
			);
			context.drawImage(
				image,
				0, 0
			);
			context.setTransform(); // Reset transformation
			
			// Overlay
			
			context.drawImage(
				overlay,
				0, 0,
				canvas.width, canvas.height
			);
		}
		
		$("#upload-image").on(
			"click",
			function (event) {
				$("#uploaded-image-hidden").click();
			}
		);
		
		// https://stackoverflow.com/a/33997042
		// Thanks to Sascha Klatt!
		function isStupidFacebookBrowser() {
			const ua = navigator.userAgent || navigator.vendor || window.opera;
			return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
		}
		
		// https://stackoverflow.com/a/50300880
		// Thanks to Ulf Aslak!
		$("#download-image").on(
			"click",
			function (event) {
				const dataURL = canvas.toDataURL();
				
				if (!isStupidFacebookBrowser()) {
					const link = document.createElement('a');
					
					link.download = "Teplaky.png";
					link.href = dataURL;
					
					link.click();
				} else {
					const form = document.createElement("form");
					
					form.action = "/fb-proxy";
					form.method = "POST";
					
					const formImage = document.createElement("input");
					formImage.type = "text";
					formImage.value = dataURL;
					formImage.id = formImage.name = "image";
					
					form.appendChild(formImage);
					
					form.style.display = "none";
					
					document.body.appendChild(form);
					form.submit();
				}
			}
		);
		
		$("#uploaded-image-hidden").on(
			"change",
			async function (event) {
				if (event.target.files.length === 0) {
					return;
				}
				
				const fileReader = new FileReader();
				
				const readPromise = new Promise(
					resolve => {
						fileReader.onloadend = function(event) {
							image = new Image();
							
							image.onload = function() {
								redraw();
								resolve();
							}
							
							image.src = event.target.result;
						}
						
						fileReader.readAsDataURL(event.target.files[0]);
					}
				);
				
				await readPromise;
			}
		);
		
		redraw();
	}
);
