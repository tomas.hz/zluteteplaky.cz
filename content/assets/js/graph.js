const ctx = document.getElementById("earning-canvas").getContext("2d");
const chart = new Chart(ctx, {
	type: "line",
	data: {
		labels: [
			"2012",
			"2013",
			"2014",
			"2015",
			"2016",
			"2017",
			"2018",
			"2019",
			"2020"
		],
		datasets: [{

			// Dotace celkem
			label: "Dotace celkem",
			data: [
				955200000,
				1127100000,
				1653800000,
				1645500000,
				1878700000,
				2054800000,
				1916900000,
				1819000000,
				1672500000
			],

			// Color
			fill: false,
			backgroundColor: [
				"#FFCC00"
			],
			borderColor: [
				"#FFCC00"
			],
			borderWidth: 5,
			pointBorderColor: "#FFCC00"
		},
		{
			// Provozní dotace
			label: "Provozní dotace",
			data: [
				774100000,
				811400000,
				1086100000,
				1137500000,
				1367900000,
				1341900000,
				1606100000,
				1546600000,
				1452100000
			],

			//color
			fill: false,
			backgroundColor: [
				"#7C7675"
			],
			borderColor: [
				"#7C7675"
			],
			borderWidth: 5,
			pointBorderColor: "#7C7675"
		},
		{
			// Investiční ­ dotace
			label: "Investiční­ dotace",
			data: [
				181100000,
				315700000,
				596700000,
				508000000,
				510800000,
				712900000,
				310800000,
				272400000,
				220400000
			],

			// Color
			fill: false,
			backgroundColor: [
				"#000000"
			],
			borderColor: [
				"#000000"
			],
			borderWidth: 5,
			pointBorderColor: "#000000"
		}],
		
	},
	options: {
		responsive: true,
		tooltips: {
			mode: "point"
		}
	}
});
