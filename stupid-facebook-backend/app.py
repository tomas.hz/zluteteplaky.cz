"""Stupid inbuilt Facebook browser workaround. Because of course you need one.
That is, if you want to use that horrid spyware "social" "medium" in the first
place."""

import base64
import binascii
import http.client
import io

import flask
import flask_limiter
import flask_limiter.util
import werkzeug.exceptions

app = flask.Flask(__name__)
limiter = flask_limiter.Limiter(
	app,
	key_func=lambda: (
		flask.request.headers.get(
			"X-Forwarded-For",
			flask_limiter.util.get_remote_address()
		)
	),
	default_limits=["5/1day"]
)

@app.route("/generate", methods=["POST"])
def generate():
	image_data_url = flask.request.form.get("image")
	
	if (
		image_data_url is None or
		not image_data_url.startswith("data:image/png;base64,")
	):
		raise werkzeug.exceptions.BadRequest
	
	split_content = image_data_url.split("data:image/png;base64,")
	
	if len(split_content) != 2:
		raise werkzeug.exceptions.BadRequest
	
	base64_content = split_content[1]
	
	try:
		content = base64.b64decode(
			base64_content,
			validate=True
		)
	except (binascii.Error, ValueError):
		raise werkzeug.exceptions.BadRequest
	
	image_io = io.BytesIO(content)
	
	return flask.send_file(
		image_io,
		mimetype="image/png",
		as_attachment=True,
		download_name="Teplaky.png"
	), http.client.OK
